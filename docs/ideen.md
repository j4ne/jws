`monospaced`... Interfaces

# Ideen
## Was soll JWS können?
alles
# Umsetzung
## Klassen
### Library
noch nix
### Mitarbeitersystem
* `Person`
    * Mitarbeiter
        * Hat "Typ variable (Im idealfall ENUM)"
        * Hat Methoden die überprüfen ob ein Mitarbeiter z.b. putzen kann (wenn sein typ reinigungskraft ist)


### Gütersystem
#### Güterdefinitionen (ein mal)
* `ProductType`
    * Kategorien mit spezialisierung (Ablaufdatum bei Lebensmitteln)
    * Instanzen haben Name, id, preis pro einheit, evtl einheitsgröße(bei lebensmittel z.b. haltbarkeitsdauer)
    
#### Güter (mit anzahl usw)

##### Möglichkeit 1: 3Dimensionale Array/Listenkombination

    * Product
        * hat Kaufdatum, menge (vllt noch mehr)
    * Liste der Filialen/Lager
        * Array mit ProduktIDs als index
            * Product Liste, also Einkäufe/Pakete (z.B. wenn man 15kg Schinken kauft wird das als ein Eintrag gespeichert, bei 10 an 1 tag und 10 am zweiten Tag sind es 2 Einträge wegen Ablaufdatum und so)
            

* **Vorteile**
    * Schnelles Filtern, z.B. bei schinken muss man nur den 2. index auf die Id von Schinken setzen
    * Arraymäßige Trennung von Lagern und Produkten
* **Nachteile**
    * Könnte (also sehr warscheinlich) schnell wegen den 3 Dimensionen und den Listen unübersichtlich werden
    * In den Endelementen stehen nicht direkt infos über das Produkt (könnte man leicht beheben indem man bei Product einfach die ProduktID reinschreibt)
    
    
##### Möglichkeit 2: Große Liste/Array
    * Product
        * hat Kaufdatum,menge,produktID,lagerID (vllt mehr)
    * Großes Array/Liste mit Products (Infos über lager,produkt in den Elementen und nicht den Indizes!)
* **Vorteile**
    * Streams leichter verwendbar (Bei Methode 1 evtl auch??? kp)
    * Alle infos in Endelement
    * Code leichter lesbar weil keine Mehrdimensionalen Sachen verwendet werden
* **Nachteile**
    * Ganz viele Sachen nebeneinander -> eventuell unübersichtlich beim debuggen
    * Performance sinkt mindestens ein bisserl, weil Nicht direkt gefiltert werden kann wie bei Methode 1


##### Möglichkeit 3: Datenbank
    * Einfache Datenbank mit ProduktIDs
    * Evtl auch zweite Tabelle in DB mit Produktdefinitionen für 
    INNER JOIN oder so... (Wie macht man dann Produktdefinitionen über file??)
* **Vorteile**
    * Sehr gute übersichtlichkeit, nur SQL Abfragen(Könnte man in Konstanten speichern für noch mehr übersichtlichkeit)
    * Speichern Leicht, wird "Automatisch" gemacht
* **Nachteile**
    * Performance fragwürdig, weil filezugriffe? (Werden da überhaupt filezugriffe gemacht oder wird die Datenbank im RAM gecached?)
    * Datenbanken hamma noch ned gemacht in pos und so (KP wie schwer des in Java ist)
    
    
##### Möglichkeit Enk: Krebs
    * Produkt mit "Barcode"
    * Barcode ist int/long: ersten 3 stellen verweist auf typ
    * Rest ist für Nummer des Kaufs
    * Enthält auch Ablaufdatum oder so
* **Nachteile**
    * Enk... nein