package org.janedoe.jws.lib;

import java.io.*;

public class Helpful {
	public static String readLineOutOfFile(File file, int lineNumber){
		if(lineNumber < 1){ //Überprüfen
			throw new IllegalArgumentException("readLineOutOfFile wurde eine ungültige lineNumber übergeben: " + lineNumber);
		}

		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			for (int i = 1; i <= lineNumber; i++) {
				line = br.readLine();
				if(line == null){
					throw new IllegalArgumentException("readLineOutOfFile wurde eine ungültige lineNumber übergeben: " + lineNumber);
				}
			}
			return line;
		}catch (FileNotFoundException e){
			System.err.println(e.getMessage());
		}catch (IOException io){
			System.err.println(io.getMessage());
		}

		return "";
	}

	public static String readLineOutOfFile(String url, int lineNumber){
		File file = new File(url);
		return readLineOutOfFile(file, lineNumber);
	}
}
