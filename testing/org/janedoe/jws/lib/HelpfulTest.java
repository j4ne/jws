package org.janedoe.jws.lib;

import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class HelpfulTest {
	@Test
	void testReadLineOutOfFile() {
		assertEquals("Mueller", Helpful.readLineOutOfFile("data/nachnamen.dat", 1));
		assertEquals("Rademacher", Helpful.readLineOutOfFile("data/nachnamen.dat", 613));
		assertEquals("Sophia", Helpful.readLineOutOfFile("data/vornamen.dat", 67));

		String test = "ABC";
		try{
			test = Helpful.readLineOutOfFile("data/vornamen.dat", 21321);
			test = Helpful.readLineOutOfFile("data/vornamen.dat", -23);
		}catch(IllegalArgumentException ex){
			System.out.println("Richtig behandelt");
		}
		assertEquals("ABC", test);

		File file = new File("data/nachnamen.dat");
		assertEquals("Rademacher", Helpful.readLineOutOfFile(file, 613));
	}
}